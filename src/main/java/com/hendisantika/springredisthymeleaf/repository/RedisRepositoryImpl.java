package com.hendisantika.springredisthymeleaf.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Repository;

import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-redis-thymeleaf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 08/01/18
 * Time: 21.02
 * To change this template use File | Settings | File Templates.
 */

@Repository
public class RedisRepositoryImpl implements RedisRepository {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public Set<String> keys() {
        return stringRedisTemplate.keys("*");
    }

    @Override
    public Map<String, String> values() {
        ValueOperations<String, String> valueOps = stringRedisTemplate.opsForValue();
        List<String> keys = new ArrayList<>(stringRedisTemplate.keys("*"));
        List<String> values = valueOps.multiGet(keys);
        int size = Math.min(keys.size(), values.size());
        Map<String, String> map = new TreeMap<>();
        for (int i = 0; i < size; i++) {
            map.put(keys.get(i), values.get(i));
        }

        return map;
    }

    @Override
    public void add(String key, String value) {
        ValueOperations<String, String> valueOps = stringRedisTemplate.opsForValue();
        valueOps.set(key, value);
    }

    @Override
    public void delete(String key) {
        stringRedisTemplate.delete(key);
    }
}
