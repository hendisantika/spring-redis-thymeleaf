package com.hendisantika.springredisthymeleaf.repository;

import java.util.Map;
import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-redis-thymeleaf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 08/01/18
 * Time: 21.01
 * To change this template use File | Settings | File Templates.
 */
public interface RedisRepository {
    /**
     * Return a set of keys in Redis: all of them.
     *
     * @return non null.
     */
    Set<String> keys();

    /**
     * Return key-value pairs in Redis: all of them.
     *
     * @return non null.
     */
    Map<String, String> values();

    /**
     * Add key-value pair to Redis.
     */
    void add(String key, String value);

    /**
     * Delete a key-value pair in Redis.
     */
    void delete(String key);
}
