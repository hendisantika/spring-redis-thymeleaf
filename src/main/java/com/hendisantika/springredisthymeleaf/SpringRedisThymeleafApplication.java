package com.hendisantika.springredisthymeleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringRedisThymeleafApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringRedisThymeleafApplication.class, args);
	}
}
